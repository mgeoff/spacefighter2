
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}

//update method for EnemyShips that returns nothing and takes GameTime as a parameter
void EnemyShip::Update(const GameTime *pGameTime)
{
	if (m_delaySeconds > 0) //if the delaySeconds is more than zero
	{//subtract the number of seconds the game has been running from the delay seconds
		m_delaySeconds -= pGameTime->GetTimeElapsed();

		if (m_delaySeconds <= 0)
		{//once the delay is 0 or less, activate the GameObject (in this case the enemy ship)
			GameObject::Activate();
		}
	}

	if (IsActive())
	{
		m_activationSeconds += pGameTime->GetTimeElapsed();
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}

	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}