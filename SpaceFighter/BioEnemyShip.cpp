
#include "BioEnemyShip.h"


BioEnemyShip::BioEnemyShip()
{
	SetSpeed(150);
	SetMaxHitPoints(1);
	SetCollisionRadius(20);

}

//returns nothing, is an update method for BioEnemyShip that takes GameTime as a parameter
void BioEnemyShip::Update(const GameTime *pGameTime) 
{
	if (IsActive()) //if a given ship is active
	{ //declare a float called x and use a sin function to make the fancy back and forth animation
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f; //x is the value along the x axis
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed()); //and the y axis is determined by time and ship speed

		if (!IsOnScreen()) Deactivate(); //deactivate the ship if it is off the screen
	}

	EnemyShip::Update(pGameTime); //then send the game time to the enemyship update function
}


void BioEnemyShip::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}
